import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import { Grid, Form, Menu, Segment, Progress, Rating, Header, Icon, Image, Card, Button, Comment } from 'semantic-ui-react'
import CardComponent from './components/Cards.js';
import CommentsComponent from './components/Comments.js';
import ProgressComponent from './components/Progress.js';
import TransitionsComponent from './components/Transitions.js';
import PortalsComponet from './components/Portals.js';
import PaginationComponent from './components/Paginations.js';
import DeveloperComponent from './components/Developer.js';



class App extends Component {
  state = { activeItem: 'Developer' }

  handleItemClick = (e, { name }) => this.setState({ activeItem: name })

  render() {
    const { activeItem } = this.state

    return (
      <div className="App">
        <div className="header">
          <Image src='logo.png' size='small' />
          <div>Developer Sandbox  ☀️🕶️🏝️</div>

        </div>

        <div className="body">
          <Grid>
            <Menu inverted vertical tabular id="menu">
              <Menu.Item name='Developer' active={activeItem === 'Developer'} onClick={this.handleItemClick} />
              <Menu.Item name='Cards' active={activeItem === 'Cards'} onClick={this.handleItemClick} />
              <Menu.Item name='Comments' active={activeItem === 'Comments'} onClick={this.handleItemClick} />
              <Menu.Item name='Progress' active={activeItem === 'Progress'} onClick={this.handleItemClick} />
              <Menu.Item name='Transitions' active={activeItem === 'Transitions'} onClick={this.handleItemClick} />
              <Menu.Item name='Pagination' active={activeItem === 'Pagination'} onClick={this.handleItemClick} />
              <Menu.Item name='Portals' active={activeItem === 'Portals'} onClick={this.handleItemClick} />

            </Menu>

            <Grid.Column stretched width={12}>
              <Segment hidden={activeItem !== "Cards"}>
                <CardComponent />
              </Segment>

              <Segment hidden={activeItem !== "Comments"}>
                <CommentsComponent />
              </Segment>

              <Segment hidden={activeItem !== "Progress"}>
                <ProgressComponent />
              </Segment>

              <Segment hidden={activeItem !== "Transitions"}>
                <TransitionsComponent />
              </Segment>

              <Segment hidden={activeItem !== "Portals"}>
                <PortalsComponet />
              </Segment>

              <Segment hidden={activeItem !== "Pagination"}>
                <PaginationComponent />
              </Segment>

              <Segment hidden={activeItem !== "Developer"}>
                <DeveloperComponent />
              </Segment>

            </Grid.Column>

          </Grid>

          <a href="https://react.semantic-ui.com/" target="_blank">
            <Segment color='teal'>Modules used from Semantic UI</Segment>
          </a>
        </div>
      </div>
    );
  }
}

export default App;
