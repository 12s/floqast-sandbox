import React, { Component } from 'react';
import { Segment, Header, Progress, Button } from 'semantic-ui-react'

class ProgressComponent extends Component {
  state = { percent: 33 }

  increment = () =>
    this.setState({
      percent: this.state.percent >= 100 ? 0 : this.state.percent + 20,
  })

  render() {
    return (
      <React.Fragment>
        <Segment inverted>
          <Header as='h4' inverted color="teal">
            Progress Bars
          </Header>
          <Segment>
            <div>
              <Progress percent={this.state.percent} indicating />
              <Button onClick={this.increment}>Increment</Button>
            </div>
          </Segment>
          <Header as='h4' inverted color="teal">
            Warning Bars
          </Header>
          <Segment>

            <Progress percent={100} warning>
              I am warning you
            </Progress>
          </Segment>

          <Header as='h4' inverted color="teal">
            Error Bars
          </Header>
          <Segment>
            <Progress percent={100} error>
              There was an error
            </Progress>
          </Segment>
        </Segment>
      </React.Fragment>
    );
  }
}

export default ProgressComponent;
