import _ from 'lodash'
import React, { Component } from 'react';
import { Segment, Header, Transition, Button, List, Image, Grid, Form } from 'semantic-ui-react'

const users = ['ade', 'chris', 'christian', 'daniel', 'elliot', 'helen']
const transitions = ['jiggle', 'flash', 'shake', 'pulse', 'tada', 'bounce', 'glow']
const options = transitions.map(name => ({ key: name, text: name, value: name }))

export default class TransitionsComponent extends Component {
  state = { items: users.slice(0, 3), animation: transitions[0], duration: 500, visible: true }

  handleAdd = () => this.setState({ items: users.slice(0, this.state.items.length + 1) })

  handleRemove = () => this.setState({ items: this.state.items.slice(0, -1) })

  handleChange = (e, { name, value }) => this.setState({ [name]: value })

  toggleVisibility = () => this.setState({ visible: !this.state.visible })

  render() {
    const { items, animation, duration, visible } = this.state

    return (
      <React.Fragment>
        <Segment inverted>
          <Header as='h4' inverted color="teal">
            Transition Groups
          </Header>
          <div>
            <Button.Group>
              <Button disabled={items.length === 0} icon='minus' onClick={this.handleRemove} />
              <Button disabled={items.length === users.length} icon='plus' onClick={this.handleAdd} />
            </Button.Group>

            <Transition.Group as={List} duration={200} divided size='huge' verticalAlign='middle'>
              {items.map(item => (
                <List.Item key={item}>
                  <Image avatar src={`./images/avatar/small/${item}.jpg`} />
                  <List.Content header={_.startCase(item)} />
                </List.Item>
              ))}
            </Transition.Group>
          </div>
        </Segment>

        <Segment inverted>
          <Header as='h4' inverted color="teal">
            Static Animations
          </Header>

          <Grid columns={2}>
            <Grid.Column as={Form}>
              <Form.Select
                label='Choose transition'
                name='animation'
                onChange={this.handleChange}
                options={options}
                value={animation}
              />
              <Form.Input
                label={`Duration: ${duration}ms `}
                min={100}
                max={2000}
                name='duration'
                onChange={this.handleChange}
                step={100}
                type='range'
                value={duration}
              />
              <Form.Button content='Run' onClick={this.toggleVisibility} />
            </Grid.Column>

            <Grid.Column>
              <Transition animation={animation} duration={duration} visible={visible}>
                <Image centered size='small' src='https://react.semantic-ui.com/images/leaves/5.png' />
              </Transition>
            </Grid.Column>
          </Grid>
        </Segment>


      </React.Fragment>
    );
  }
}
