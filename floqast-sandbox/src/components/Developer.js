import React, { Component } from 'react';
import { Segment, Header } from 'semantic-ui-react'

class ProgressComponent extends Component {
  render() {
    return (
      <React.Fragment>
        <Segment inverted>
          <Header as='h4' inverted color="teal">
            Thank you!
          </Header>
          Thank you for considering me for the front end developer position.<br/><br/>
          While I may not have been the right fit for the position, I hope the new year will bring new jobs I can be considered for.

          <br/><br/>
          mattdevoss@gmail<br/>
          <a href="https://www.linkedin.com/in/mattdevoss/" target="_blank">LinkedIn</a>

          <br/><br/>
          Site made with React. I didn't have the greatest idea for a demo application, but maybe some of the Semantic UI modules may inspire future streamlined solutions.
          <br/><br/>
          Repository link <a target="_blank" href="https://gitlab.com/12s/floqast-sandbox">here</a>
          <br/>
          Modules chosen from <a target="_blank" href="https://react.semantic-ui.com/"> Semantic UI</a>
        </Segment>
      </React.Fragment>
    );
  }
}

export default ProgressComponent;
